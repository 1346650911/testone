from django.db import models
from django.utils import timezone


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField(default=timezone.now)

    class meta:
        ordering = ('-pub_date',)

    def __str__(self):
        return self.title


# class Test(models.Model):
#     change = models.CharField(max_length=10)
#     number = models.IntegerField()
#     class meta:
#         managed = True
#     def __str__(self):
#         return self.change
